const express=require('express');
const path=require('path');
var fs = require('fs'); 
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const sequelize=require(__dirname + '/public/js/baza.js');
const app=express();

app.use('/', express.static(__dirname + '/public'));
app.use('/', express.static(__dirname + '/public/html'));
app.use('/', express.static(__dirname + '/public/css'));
app.use('/', express.static(__dirname + '/public/images'));
app.use('/', express.static(__dirname + '/public/js'));

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: true }));

const Student=require(__dirname + '/public/models/studentModel.js')(sequelize);
const Vjezba=require(__dirname + '/public/models/vjezbaModel.js')(sequelize);
const Grupa=require(__dirname + '/public/models/grupaModel.js')(sequelize);
const Zadatak=require(__dirname + '/public/models/zadatakModel.js')(sequelize);
    
//Vjezba.hasMany(Zadatak,{as:'idVjezbe',onDelete:'CASCADE'});

Student.sync();
Vjezba.sync();
Grupa.sync();
Zadatak.sync(); 

app.get('/vjezbe/', async(req, res) => {

    var objekat={};
    objekat.brojVjezbi=0;
    objekat.brojZadataka=[];
    try{
      const broj = await Vjezba.count({ where: {}});
      objekat.brojVjezbi=broj;
}
     catch(error){
      console.log("Greška pri dohvaćanju broja vježbi");
     }
    
    for(let i=1;i<=objekat.brojVjezbi;i++){
      try{
        const broj = await Zadatak.count({where: {idVjezbe:i}});
        objekat.brojZadataka.push(broj);
       }
       catch(error){
        console.log("Greška pri dohvaćanju broja zadataka");
       }
    }
  res.send(objekat);
})

  app.post('/vjezbe', async(req, res) => {
    var tijelo=req.body; 
    //provjeriti parametre(broj vjezbi 1 do 15 i broj zadataka 0 do 10)
    var objekat={};
    objekat.brojVjezbi=parseInt(tijelo.brojVjezbi);
    objekat.brojZadataka=[];
    objekat.brojZadataka=tijelo.brojZadataka;

    var greske="";
    var flag=true;
    var greska=false;

    if(objekat.brojVjezbi!=objekat.brojZadataka.length){
      greska=true;
      flag=false;
      greske="Pogrešan parametar brojZadataka";
    }

    else{
    if(!(objekat.brojVjezbi>=1 && objekat.brojVjezbi<=15)){
    greske=greske+"Pogrešan parametar brojVjezbi";
    flag=false;
    greska=true;
    }
  
    for(let i=0;i<objekat.brojVjezbi;i++){
      if(!(objekat.brojZadataka[i]>=0 && objekat.brojZadataka[i]<=10)){
        greska=true;
        if(flag==true){
          greske=greske+"Pogrešan parametar ";
          flag=false;
        }
        else if(flag==false){
         greske=greske+", ";
        }
      greske=greske+"z"+i;
      }
    }
  }
    var vrati={};
    if(greska==true){
      vrati.status="error";
      vrati.data=greske;
      console.log("ne pisi vjezbe u tabelu");
    }
    else{
      //obrisati vjezbe iz tabele
      //ALTER TABLE Vjezba DROP CONSTRAINT Zadataks_ibfk_1;
      Vjezba.truncate();

      vrati=objekat;
      for(let i=1;i<=objekat.brojVjezbi;i++){
        try{
              function upsert(naziv){
               return Vjezba.create({naziv:naziv});
              }
              await upsert("VJEŽBA "+i);
        }
             catch(error){
              console.log("Greška pri dodavanju vježbi");
             }
        }

      Zadatak.truncate();
      
        //zadaci redom pripadaju vjezbama s id-evima od 1 do brojVjezbi
        for(let i=0;i<objekat.brojZadataka.length;i++){
          for(let j=1;j<=objekat.brojZadataka[i];j++){
          try{
                function upsert(naziv,idVjezbe){
                 return Zadatak.create({naziv:naziv,idVjezbe:parseInt(idVjezbe)});
                }
                await upsert("ZADATAK "+j,i+1);
          }
               catch(error){
                console.log("Greška pri dodavanju zadataka");
               }
          }
        }
  }
   
    res.send(vrati);
  })

      app.post('/student', async(req, res) => {
        var objekat={};
        var neuspjesni=0;
        var uspjesni=0;
        var stringNeuspjesnih="";
      
          var result;
          try{
            const ime=req.body.ime.trim();
            if(ime.length==0) ime=null;
            const prezime=req.body.prezime.trim();
            if(prezime.length==0) prezime=null;
            const index=req.body.index.trim();
            if(index.length==0) index=null;
            const grupa=req.body.grupa.trim();
            if(grupa.length==0) grupa=null;
      
            function upsert(ime,prezime,index,grupa){
             const find={index};
             const create={ime,prezime,index,grupa};
             //ako studenta treba kreirati i grupa nije nadjena, dodati grupu
             return Student.findOrCreate({where: find,defaults: create});
            }
            result=await upsert(ime,prezime,index,grupa);
            if(result[1]==false){
               neuspjesni=neuspjesni+1;
            }
            else{
              uspjesni=uspjesni+1;
              try{
              function upsertGrupa(naziv){
                const findGrupa={naziv};
                const termin=new Date().toLocaleString("bs-BA", {timeZone: "Europe/Belgrade"}).substring(10,16);
                const createGrupa={naziv,termin};
                //ako studenta treba kreirati i grupa nije nadjena, dodati grupu
                return Grupa.findOrCreate({where: findGrupa,defaults: createGrupa});
               }
               result2=await upsertGrupa(req.body.grupa.trim());
               if(result2[1]==false){
                 console.log("Grupa već postoji, ne treba je dodati");
               }  
            }
            catch(error2){
              console.log("Greška pri dodavanju grupe nakon dodavanja studenta");
            }
          }
           }
           catch(error){
            console.log("Greška pri dodavanju studenata");
           }
        
      
       if(neuspjesni==0) objekat.status="Dodano "+uspjesni+" studenata";
       else{
         var prvi_dio="Dodano "+uspjesni+" studenata, a studenti ";
         objekat.status=prvi_dio+stringNeuspjesnih;
         objekat.status=objekat.status+" već postoje!";
       }

       if(objekat.status=="Dodano 0 studenata") objekat.status="Polja ne mogu biti prazna!";
       else if(objekat.status=="Dodano 0 studenata, a studenti  već postoje!") { 
         objekat.status="Student sa indeksom "+req.body.index.trim()+" već postoji!";
       }
       else if(objekat.status=="Dodano 1 studenata") objekat.status="Kreiran student!";

       res.send(objekat);
      
      })

  app.get('/studenti/', (req, res) => {
    Student.findAll().then(studenti =>
      {
        res.send(studenti);
    });
  })

  app.get('/grupe/', (req, res) => {
    Grupa.findAll().then(grupa =>
      {
        res.send(grupa);
    });
  })

  app.put('/student/:index', (req, res) => {
    var objekat={};
    Student.findOne({ where: {index: req.params.index} }).then((student)=>{
      if(student==null){  //ako student sa indexom ne postoji
        objekat.status="Student sa indexom "+req.params.index+" ne postoji!";
      }
      else{//ako student postoji
        Grupa.findOne({ where: {naziv: req.body.grupa} }).then((grupa)=>{
          if(grupa==null){  //ako grupa ne postoji kreirati je
            var termin=new Date().toLocaleString("bs-BA", {timeZone: "Europe/Belgrade"}).substring(10,16);
          Grupa.create({naziv: req.body.grupa, termin:termin});
          }
        })
    //ako postoji mijenja mu se grupa
    Student.update(
      { grupa: req.body.grupa },
      { where: { index: req.params.index } }
    )
    objekat.status="Promijenjena grupa studentu "+req.params.index+".";
      }
      res.send(objekat);
  })
})

app.post('/batch/student', async(req, res) => {
  //csv tekst bez headera (ime,prezime,index,grupa)
  //dodaju se studenti u tabelu student
  var csv=req.body.split('\r\n');
  var objekat={};
  var neuspjesni=0;
  var uspjesni=0;
  var stringNeuspjesnih="";

  for(let i=0;i<csv.length;i++){
    var result;
    try{
      var student=csv[i].split(",");
      const ime=student[0].trim();
      if(ime.length==0) ime=null;
      const prezime=student[1].trim();
      if(prezime.length==0) prezime=null;
      const index=student[2].trim();
      if(index.length==0) index=null;
      const grupa=student[3].trim();
      if(grupa.length==0) grupa=null;

      function upsert(ime,prezime,index,grupa){
       const find={index};
       const create={ime,prezime,index,grupa};
       //ako studenta treba kreirati i grupa nije nadjena, dodati grupu
       return Student.findOrCreate({where: find,defaults: create});
      }
      result=await upsert(ime,prezime,index,grupa);
      if(result[1]==false){
        if(neuspjesni==0){
          stringNeuspjesnih=stringNeuspjesnih+student[2].trim();
        }
        else{
          stringNeuspjesnih=stringNeuspjesnih+","+student[2].trim();
        }
         neuspjesni=neuspjesni+1;
      }
      else{
        uspjesni=uspjesni+1;
        try{
        function upsertGrupa(naziv){
          const findGrupa={naziv};
          const termin=new Date().toLocaleString("bs-BA", {timeZone: "Europe/Belgrade"}).substring(10,16);
          const createGrupa={naziv,termin};
          //ako studenta treba kreirati i grupa nije nadjena, dodati grupu
          return Grupa.findOrCreate({where: findGrupa,defaults: createGrupa});
         }
         result2=await upsertGrupa(student[3].trim());
         if(result2[1]==false){
           console.log("Grupa već postoji, ne treba je dodati");
         }  
      }
      catch(error2){
        console.log("Greška pri dodavanju grupe nakon batch dodavanja studenta");
      }
    }
     }
     catch(error){
      console.log("Greška pri batch dodavanju studenata");
     }
  }

 if(neuspjesni==0) objekat.status="Dodano "+uspjesni+" studenata";
 else{
   var prvi_dio="Dodano "+uspjesni+" studenata, a studenti ";
   objekat.status=prvi_dio+stringNeuspjesnih;
   objekat.status=objekat.status+" već postoje!";
 }

 res.send(objekat);
}
)

app.listen(3000);
