FROM node:latest

WORKDIR /home/jasmina/wt21p18491

COPY ./package*.json ./
RUN npm install

COPY . .

EXPOSE 8080
CMD ["node", "index.js"]
