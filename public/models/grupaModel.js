const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Grupa=sequelize.define('Grupa', {
        naziv:{
            type:Sequelize.STRING,
            unique:true,
            allowNull:false
        },
        termin:{
            type:Sequelize.STRING,
            allowNull:false
        }
        })
        return Grupa;
};

