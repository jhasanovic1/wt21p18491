const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Student=sequelize.define('Student', {
        ime:{
            type:Sequelize.STRING,
            allowNull: false
        },
        prezime:{
            type:Sequelize.STRING,
            allowNull: false
        },
        index:{
            type:Sequelize.STRING,
            unique:true,
            allowNull: false
        },
        grupa:{
            type:Sequelize.STRING,
            allowNull: false
        }
        })
        return Student;
};


