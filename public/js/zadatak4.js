const json1=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 2,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 0,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "]\n"+
    "}\n";


const json2=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 3,\n"+
          "\"passes\": 1,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+

        "{\n"+
        "\"title\": \"T3\",\n"+
        "\"fullTitle\": \"T3 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T2\",\n"+
         "\"fullTitle\": \"T2 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

      "]\n"+
    "}\n";

    const json3=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 4,\n"+
          "\"passes\": 1,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 3,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

             "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

                    
          "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T4\",\n"+
         "\"fullTitle\": \"T4 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

      "]\n"+
    "}\n";

    


const json4=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 0,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T4\",\n"+
         "\"fullTitle\": \"T4 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": []\n"+
    "}\n";


const json5=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 3,\n"+
          "\"passes\": 1,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T3\",\n"+
         "\"fullTitle\": \"T3 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+

         "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

      "]\n"+
    "}\n";




 const json6=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 4,\n"+
          "\"passes\": 2,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

             "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

                    
          "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T4\",\n"+
         "\"fullTitle\": \"T4 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+

         "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+

      "]\n"+
    "}\n";


   const json7=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 0,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": []\n"+
    "}\n";

  const json8=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 0,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T4\",\n"+
         "\"fullTitle\": \"T4 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": []\n"+
    "}\n";

const json9=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 3,\n"+
          "\"passes\": 2,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 1,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T2\",\n"+
         "\"fullTitle\": \"T2 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+
         

      "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+

         "{\n"+
        "\"title\": \"T4\",\n"+
        "\"fullTitle\": \"T4 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "]\n"+
    "}\n";


const json10=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 1,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 1,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

      "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T4\",\n"+
         "\"fullTitle\": \"T4 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

         "]\n"+
    "}\n";



    let assert = chai.assert;
    describe('TestoviParser', function() {
    describe('porediRezultate()', function() {

    it('Slučaj 1-Isti broj testova, identični testovi', function() {
     var rezultat = TestoviParser.porediRezultate(json3,json6);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"promjena":"50%","greske":["T1 description","T3 description"]}');
   });

    it('Slučaj 2-Isti broj testova, različiti testovi', function() {
     var rezultat = TestoviParser.porediRezultate(json1,json4);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"promjena":"100%","greske":["T2 description","T4 description"]}');
   });

     it('Slučaj 3-Isti broj testova, različiti testovi', function() {
     var rezultat = TestoviParser.porediRezultate(json7,json8);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"promjena":"100%","greske":["T1 description","T4 description"]}');
   });
   
   it('Slučaj 4-Isti broj testova, različiti testovi-Primjer s foruma', function() {
     var rezultat = TestoviParser.porediRezultate(json2,json9);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"promjena":"50%","greske":["T3 description","T2 description"]}');
   });
   
    it('Slučaj 5-Različit broj testova, različiti testovi', function() {
     var rezultat = TestoviParser.porediRezultate(json5,json10);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"promjena":"75%","greske":["T2 description","T3 description","T1 description"]}');
   });



 });
});
