const json1=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 2,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 0,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "]\n"+
    "}\n";

const json2=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 3,\n"+
          "\"passes\": 2,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 1,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

      "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T2\",\n"+
         "\"fullTitle\": \"T2 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T3\",\n"+
        "\"fullTitle\": \"T3 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "]\n"+
    "}\n";

    const json3=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 4,\n"+
          "\"passes\": 1,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 3,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

             "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+
                    
          "{\n"+
              "\"title\": \"T4\",\n"+
              "\"fullTitle\": \"T4 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

                    
          "{\n"+
              "\"title\": \"T3\",\n"+
              "\"fullTitle\": \"T3 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
         "\"passes\": [\n"+
         "{\n"+
         "\"title\": \"T4\",\n"+
         "\"fullTitle\": \"T4 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "}\n"+

      "]\n"+
    "}\n";

const json4=
"{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 0,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": []\n"+
    "}\n";

const json5=
"{{\n"+
          "\"stats\": {\n" +
          "\"suites\": 2,\n"+
          "\"tests\": 2,\n"+
          "\"passes\": 0,\n"+
          "\"pending\": 0,\n"+
          "\"failures\": 2,\n"+
          "\"start\": \"2021-11-05T15:00:26.343Z\",\n"+
          "\"end\": \"2021-11-05T15:00:26.352Z\",\n"+
          "\"duration\": 9\n"+
        "},\n"+

        "\"tests\": [\n"+
          "{\n"+
              "\"title\": \"T1\",\n"+
              "\"fullTitle\": \"T1 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 1,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "},\n"+

            "{\n"+
              "\"title\": \"T2\",\n"+
              "\"fullTitle\": \"T2 description\",\n"+
              "\"file\": null,\n"+
              "\"duration\": 0,\n"+
              "\"currentRetry\": 0,\n"+
              "\"speed\": \"fast\",\n"+
              "\"err\": {}\n"+
            "}\n"+
          
         "],\n"+
          
         "\"pending\": [],\n"+
         "\"failures\": [\n"+
         "{\n"+
         "\"title\": \"T1\",\n"+
         "\"fullTitle\": \"T1 description\",\n"+
         "\"file\": null,\n"+
         "\"duration\": 1,\n"+
         "\"currentRetry\": 0,\n"+
         "\"speed\": \"fast\",\n"+
         "\"err\": {}\n"+
         "},\n"+
         

        "{\n"+
        "\"title\": \"T2\",\n"+
        "\"fullTitle\": \"T2 description\",\n"+
        "\"file\": null,\n"+
        "\"duration\": 0,\n"+
        "\"currentRetry\": 0,\n"+
        "\"speed\": \"fast\",\n"+
        "\"err\": {}\n"+
       "}\n"+

      "],\n"+
         "\"passes\": []\n"+
    "}\n";

    
    let assert = chai.assert;
    describe('TestoviParser', function() {
    describe('dajTacnost()', function() {
    it('Slučaj 1 - Svi testovi prolaze, tačnost je 100%', function() {
     var rezultat = TestoviParser.dajTacnost(json1);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"tacnost":"100%","greske":[]}');
   });

   it('Slučaj 2 - Jedan test od ukupno 3 pada, tačnost je 66.7%', function() {
     var rezultat = TestoviParser.dajTacnost(json2);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"tacnost":"66.7%","greske":["T1 description"]}');
     });

   it('Slučaj 3 - Padaju 3 testa od ukupno 4, tačnost je 25%', function() {
     var rezultat = TestoviParser.dajTacnost(json3);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"tacnost":"25%","greske":["T1 description","T2 description","T3 description"]}');
     });

   it('Slučaj 4 - Padaju svi testovi, tačnost je 0%', function() {
     var rezultat = TestoviParser.dajTacnost(json4);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"tacnost":"0%","greske":["T1 description","T2 description"]}');
     });

   it('Slučaj 5 - Testovi se ne mogu pokrenuti, tačnost je 0%', function() {
     var rezultat = TestoviParser.dajTacnost(json5);
     var rezultatJSON=JSON.stringify(rezultat);
     assert.equal(rezultatJSON, '{"tacnost":"0%","greske":["Testovi se ne mogu izvršiti"]}');
     });



 });
});
