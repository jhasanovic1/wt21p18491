var VjezbeAjax =(function(){

var zadnjiKlik=-1;
var klikovi=[];

var dodajInputPolja = function(DOMelementDIVauFormi,brojVjezbi){
  DOMelementDIVauFormi.innerHTML = "";
  if((brojVjezbi>=1 && brojVjezbi<=15)){
    for (let i = 0; i < brojVjezbi; i++) {
      var labela = document.createElement("LABEL");
      labela.innerHTML = 'z'+i;
      //labela.id="z"+i;
      var unos = document.createElement("INPUT");
      unos.setAttribute("type", "number");
      unos.id="z"+i;
      
      var red=document.createElement("p");

      red.appendChild(labela);
      red.appendChild(unos);
      DOMelementDIVauFormi.appendChild(red);
    }
  }
  else{
    console.log("Nemoguće dodati polja - Broj vježbi mora biti između 1 i 15!");
  }
}

var posaljiPodatke = function(vjezbeObjekat,callbackFja){
  //callbackFja ima 2 parametra: error i data
  //error sadrzi tekst greske ili null ako je ok
  //data sadrzi podatke o kreiranoj vjezbi ili null ako je bilo greske
  var ajax = new XMLHttpRequest();
  ajax.onreadystatechange = function() {
    if (ajax.readyState == 4 && ajax.status == 200)
      console.log('OK');
    if (ajax.readyState == 4 && ajax.status == 404)
    console.log('Greška u posaljiPodatke');
  }

  ajax.open("POST","http://localhost:3000/vjezbe",true);  //putem Ajax-a na rutu POST/vjezbe salje se vjezbeObjekat
  ajax.setRequestHeader("Content-Type", "application/json");
  ajax.send(JSON.stringify(vjezbeObjekat));
}

var dohvatiPodatke = function(callbackFja){
var ajax = new XMLHttpRequest();
ajax.onreadystatechange = function() {
	if (ajax.readyState == 4 && ajax.status == 200){
    console.log('Podaci dohvaćeni');
    let data=JSON.parse(ajax.responseText);
    //validacija podataka o vjezbama i zadacima
    let vjezbe=data.brojVjezbi;
    let zadaci=data.brojZadataka;
    if(!(vjezbe>=1 && vjezbe<=15) || zadaci.length!=vjezbe || zadaci.find(element => (element <0 || element>10))){
      console.log('Greška u dohvatiPodatke');
    callbackFja(data,null)
    }
    else
    callbackFja(null,data);
  }
	if (ajax.readyState == 4 && ajax.status == 404){
    console.log('Greška u dohvatiPodatke');
    callbackFja(JSON.parse(ajax.responseText),null);
  }
}
ajax.open("GET", "http://localhost:3000/vjezbe", true);
ajax.send();
}

var iscrtajVjezbe=function(divDOMelement,objekat){
for(let i=1;i<=objekat.brojVjezbi;i++){
      let vjezba = document.createElement("li");
      vjezba.id="v"+i;
      vjezba.setAttribute('title', "VJEŽBA "+i);
      vjezba.innerHTML=vjezba.title;
      vjezba.className="vjezbe-item";
      divDOMelement.appendChild(vjezba);
      vjezba.onclick = function(){ 

        if(zadnjiKlik!=vjezba.id && zadnjiKlik!=-1){
          var idZadataka="zadaci-list-"+zadnjiKlik;
          var x = document.getElementById(idZadataka);

          if (x.style.display === "none") {
            x.style.display = "block";
          } else {
            x.style.display = "none";
          }  
        }
        
        if((zadnjiKlik!=vjezba.id && !klikovi.includes(vjezba.id)) || zadnjiKlik==-1){
          var container=vjezba;
          var ul = document.createElement('ul');
          ul.className="zadaci-list";
          ul.id="zadaci-list-"+vjezba.id;
          container.appendChild(ul);
          iscrtajZadatke(ul,objekat.brojZadataka[i-1]);
        }
        if(klikovi.includes(vjezba.id)){
          var idZadataka2="zadaci-list-"+vjezba.id;
          var y=document.getElementById(idZadataka2);
          y.style.display="block";
        }

        if(!klikovi.includes(vjezba.id)) klikovi.push(vjezba.id);
        zadnjiKlik=vjezba.id;
      };
}
}

var iscrtajZadatke=function(vjezbaDOMelement,brojZadataka){

for(let i=1;i<=brojZadataka;i++){
  var z=document.createElement("li");
  z.id="z"+i;
  z.setAttribute('title', "ZADATAK "+i);
  z.setAttribute('class', "zadaci-item");
  z.innerHTML="ZADATAK "+i;
  vjezbaDOMelement.appendChild(z);
}
}


return {
  dodajInputPolja:dodajInputPolja,
  posaljiPodatke: posaljiPodatke,
  dohvatiPodatke: dohvatiPodatke,
  iscrtajVjezbe: iscrtajVjezbe
  }
  }());