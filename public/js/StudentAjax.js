var StudentAjax =(function(){

    var dodajStudenta = function(studentObjekat,fnCallback){
      var ajax = new XMLHttpRequest();
      ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 && ajax.status == 200){
          console.log('OK');
          fnCallback(null, JSON.parse(ajax.responseText));
        }
          
        if (ajax.readyState == 4 && ajax.status == 404)
        console.log('Greška u dodajStudenta');
      }
    
      ajax.open("POST","http://localhost:3000/student",true);
      ajax.setRequestHeader("Content-Type", "application/json");
      ajax.send(JSON.stringify(studentObjekat));
    }
    
    
    var iscrtajStudente=function(container,studenti){
    
      for(let i=0;i<studenti.length;i++){
        var grid_item=document.createElement("div");
        var ime=document.createElement("p");
        ime.id="ime"+studenti[i].ime;
        var prezime=document.createElement("p");
        prezime.id="prezime"+studenti[i].index;
        var index=document.createElement("p");
        index.id="index"+studenti[i].index;
        grid_item.setAttribute('class', "grid_item");
        grid_item.style.maxWidth = "50%";
        grid_item.style.maxHeight = "25%";
        ime.innerHTML=studenti[i].ime;
        prezime.innerHTML=studenti[i].prezime;
        index.innerHTML=studenti[i].index;
        grid_item.appendChild(ime);
        grid_item.appendChild(prezime);
        grid_item.appendChild(index);
        container.appendChild(grid_item);
      }
      }
      var dohvatiStudente = function(callbackFja){
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
            console.log('Podaci o studentima dohvaćeni');
            let data=JSON.parse(ajax.responseText);
            callbackFja(null,data);
          }
            if (ajax.readyState == 4 && ajax.status == 404){
            console.log('Greška u dohvatiStudente');
            callbackFja(JSON.parse(ajax.responseText),null);
          }
        }
        ajax.open("GET", "http://localhost:3000/studenti/", true);
        ajax.send();
        }

       
          var postaviGrupu = function(index,grupa,fnCallback){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
              if (ajax.readyState == 4 && ajax.status == 200){
                console.log('OK');
                fnCallback(null, JSON.parse(ajax.responseText));
              }
              if (ajax.readyState == 4 && ajax.status == 404)
              console.log('Greška u postaviGrupu');
            }
            var objekat={};
            objekat.grupa=grupa;
            ajax.open("PUT","http://localhost:3000/student/"+index,true);
            ajax.setRequestHeader("Content-Type", "application/json");
            ajax.send(JSON.stringify(objekat));
          }

          var dohvatiGrupe = function(callbackFja){
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {
                if (ajax.readyState == 4 && ajax.status == 200){
                console.log('Podaci o grupama dohvaćeni');
                let data=JSON.parse(ajax.responseText);
                callbackFja(null,data);
              }
                if (ajax.readyState == 4 && ajax.status == 404){
                console.log('Greška u dohvatiGrupe');
                callbackFja(JSON.parse(ajax.responseText),null);
              }
            }
            ajax.open("GET", "http://localhost:3000/grupe/", true);
            ajax.send();
            }

            var iscrtajTabeluGrupe=function(container,grupe){
    
              for(let i=0;i<grupe.length;i++){
                var red=document.createElement("tr");
                var naziv=document.createElement("th");
                naziv.id="naziv"+i;
                var br_studenata=document.createElement("th");
                br_studenata.id="brs"+i;
                var termin=document.createElement("th");
                termin.id="termin"+i;

                naziv.innerHTML=grupe[i].naziv;
                br_studenata.innerHTML=0;
                termin.innerHTML=grupe[i].termin;

                red.appendChild(naziv);
                red.appendChild(br_studenata);
                red.appendChild(termin);
                container.appendChild(red);
              }
              }

              var dodajRedTabele = function(tabela){
                var i=tabela.rows.length;
                  var red=document.createElement("tr");
                  var ime=document.createElement("input");
                  ime.id="ime"+i;
                  var prezime=document.createElement("input");
                  prezime.id="prezime"+i;
                  var index=document.createElement("input");
                  index.id="index"+i;
                  var grupa=document.createElement("input");
                  grupa.id="grupa"+i;
                  

                  red.appendChild(ime);
                  red.appendChild(prezime);
                  red.appendChild(index);
                  red.appendChild(grupa);
                  tabela.appendChild(red);
              }

              var dodajBatch = function(studenti,fnCallback){
                //callbackFja ima 2 parametra: error i data
                //error sadrzi tekst greske ili null ako je ok
                //data sadrzi podatke o kreiranoj vjezbi ili null ako je bilo greske
                var ajax = new XMLHttpRequest();
                ajax.onreadystatechange = function() {
                  if (ajax.readyState == 4 && ajax.status == 200){
                    console.log('OK');
                    fnCallback(null, JSON.parse(ajax.responseText));
                  }
                  if (ajax.readyState == 4 && ajax.status == 404)
                  console.log('Greška u posaljiStudenteCSV');
                }
              
                ajax.open("POST","http://localhost:3000/batch/student",true);  
                ajax.setRequestHeader("Content-Type", "text/plain");
                ajax.send(studenti);
              }

            

    return {
      dodajStudenta: dodajStudenta,
      iscrtajStudente: iscrtajStudente,
      dohvatiStudente: dohvatiStudente,
      postaviGrupu: postaviGrupu,
      dodajBatch: dodajBatch,
      dohvatiGrupe:dohvatiGrupe,
      iscrtajTabeluGrupe:iscrtajTabeluGrupe,
      dodajRedTabele:dodajRedTabele
      }
      }());