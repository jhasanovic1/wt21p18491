function dohvatiPodatke(error,data) {
    if(data!=null){
        StudentAjax.iscrtajStudente(document.getElementById('studenti'),data)
    }
    else if(data==null)
    console.log("Callback - greška pri dohvaćanju podataka o studentima");
  }

  function dohvatiPodatkeGrupe(error,data) {
    if(data!=null){
        StudentAjax.iscrtajTabeluGrupe(document.getElementById('tb_grupe'),data)
    }
    else if(data==null)
    console.log("Callback - greška pri dohvaćanju podataka o grupama");
  }

function ucitaj(){
  StudentAjax.dohvatiStudente(dohvatiPodatke);
  StudentAjax.dohvatiGrupe(dohvatiPodatkeGrupe);
}

window.onload = ucitaj();