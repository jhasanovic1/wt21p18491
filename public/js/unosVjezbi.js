var polje = document.getElementById('brojVjezbi');
polje.value="4";
var result = document.getElementById('vjezbeContainer');
VjezbeAjax.dodajInputPolja(result,4);
polje.addEventListener('input', function(evt){VjezbeAjax.dodajInputPolja(result,parseInt(this.value,10));});

var dugme=document.getElementById('dugme');
let callbackFja = function (error,data) {};

dugme.addEventListener('click',function(evt){
    var objekat={};
    objekat.brojVjezbi=parseInt(document.getElementById('brojVjezbi').value);

    var neispravneVjezbe=false;
    if(!(objekat.brojVjezbi>=1 && objekat.brojVjezbi<=15)) neispravneVjezbe=true;

    objekat.brojZadataka=[];

    var neispravniZadaci=false;
    if(neispravneVjezbe==false){
    for(let i=0;i<objekat.brojVjezbi;i++){
        var id="z"+i;
        let el=document.getElementById(id).value;
        if(el==""|| parseInt(el)<0 || parseInt(el)>10){
            neispravniZadaci=true;
            break;
        }
        objekat.brojZadataka[i]=parseInt(document.getElementById(id).value);
    }
}
    if(neispravniZadaci==false && neispravneVjezbe==false)
    VjezbeAjax.posaljiPodatke(objekat,callbackFja);
    else
    console.log("Neispravni podaci");
});