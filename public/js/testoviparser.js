var TestoviParser =(function(){
   
    var dajTacnost = function(report){
    var tacnost;
    var greske=[];
    var obj;
    const vratiObj = new Object();
    //provjera moze li se report pretvoriti u JSON objekat, tj. da li JSON.parse baca izuzetak
    try{
        obj=JSON.parse(report);
    }
    catch(e){
        if(e instanceof SyntaxError){
        tacnost=0;
        greske[0]="Testovi se ne mogu izvr�iti";
        vratiObj.tacnost="0%";
        vratiObj.greske=greske;
        return vratiObj
        }
    }
    
    //sve ok
    tacnost=(Math.round((obj.stats.passes*100)/obj.stats.tests * 10) / 10);

    for(var i=0;i<obj.stats.failures;i++){
        greske[i]=obj.failures[i].fullTitle;
    }
    tacnost=tacnost+"%";
    vratiObj.tacnost=tacnost;
    vratiObj.greske=greske;
    return vratiObj;
    }
    

    var porediRezultate = function(rezultat1, rezultat2){
    const vratiObj = new Object();

    var obj1=JSON.parse(rezultat1);
    var obj2=JSON.parse(rezultat2);

    var testoviNazivi1=[];
    var testoviNazivi2=[];

    for(var i=0;i<obj1.tests.length;i++)
     testoviNazivi1[i]=obj1.tests[i].fullTitle;

    for(var i=0;i<obj2.tests.length;i++)
     testoviNazivi2[i]=obj2.tests[i].fullTitle;
    
     var isti=false;
     var x;
     var greske=[];
     var greske1=[];
     var greske2=[];


if(testoviNazivi1.length==testoviNazivi2.length){
    console.log("Iste duzine");
    isti=true;
     for(var i=0;i<testoviNazivi2.length;i++){
         if(!testoviNazivi1.includes(testoviNazivi2[i])){
           isti=false;
           break;
         }
     }


     if(isti==true){//identicni testovi
         x=(Math.round((obj2.stats.passes*100)/obj2.stats.tests * 10) / 10);
         for(var i=0;i<obj2.stats.failures;i++){
             greske.push(obj2.failures[i].fullTitle);
             console.log(greske[i]);
         }
         
        greske.sort();
    }

}
       if(isti==false){//razliciti testovi
        console.log("Razliciti testovi");
         var failedRez1=[];
         var failedRez2=[];

         for(var i=0;i<obj1.stats.failures;i++)
         failedRez1[i]=obj1.failures[i].fullTitle;

         for(var i=0;i<obj2.stats.failures;i++)
         failedRez2[i]=obj2.failures[i].fullTitle;

         var brojac=0;
         for(var i=0;i<obj1.stats.failures;i++){
             if(!testoviNazivi2.includes(failedRez1[i])) brojac++;
         }

         x=(brojac+obj2.stats.failures)/(brojac+obj2.stats.tests)*100;

         //failed testovi iz rez1 kojih nema u tests rezultata2
         for(var i=0;i<obj1.stats.failures;i++){
             if(!testoviNazivi2.includes(failedRez1[i])){
                greske1.push(failedRez1[i]);
             }
         }
         greske1.sort();
        

         //failed iz rez2 kojih nema u failed rez1
         for(var i=0;i<obj2.stats.failures;i++){
             if(!failedRez1.includes(failedRez2[i])){
                 greske2.push(failedRez2[i]);
             }
         }

         greske2.sort();

         greske=greske1.concat(greske2);

     }
 

     x=x+"%";
     vratiObj.promjena=x;
     vratiObj.greske=greske;
     return vratiObj;
    }
   

    return {
    dajTacnost:dajTacnost,
    porediRezultate:porediRezultate
    }
    }());